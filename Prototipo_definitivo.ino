#include <SPI.h>
#include <MFRC522.h>
#define SS_PIN 10
#define RST_PIN 9
char st[20];
#include <LiquidCrystal.h> // Adiciona a biblioteca "LiquidCrystal" ao projeto
LiquidCrystal lcd(8, 2, 7, 6, 5, 4); // Pinagem do LCD --- RS 0, EN 2, DB4=D7, DB5=D6, DB6=D5, DB7=D4
MFRC522 mfrc522(SS_PIN, RST_PIN);
//#include "TimerOne.h"

void setup()
{
  pinMode(3, OUTPUT);
  pinMode(A0, OUTPUT);
  pinMode(A1, OUTPUT);
  Serial.begin(9600);  //inicia serial
  SPI.begin();  // Inicia  SPI bus
  mfrc522.PCD_Init(); // Inicia MFRC522
  Serial.println("Aproxime o seu cartao/TAG do leitor");
  Serial.println();
  lcd.begin(16, 2); // Inicia o lcd de 16x2
  // Timer1.initialize(10000000); // Inicializa o Timer1 e configura para um período de 10 segundos
  //Timer1.attachInterrupt(lecartao); // Configura a função callback() como a função para ser chamada a cada interrupção do Timer1
}

void msg_off()
{
  lcd.clear();              // Limpa o display
  lcd.setCursor(0, 0);      // 2 = 2 colunas para a direita. 0 = Primeira linha
  lcd.print("Desativando");
  lcd.setCursor(0, 1);
  lcd.print(" Sistema"); // Imprime um texto
  delay(1000);              // 5 segundos de delay

}

void msg_off_proj()
{
  lcd.clear();              // Limpa o display
  lcd.setCursor(0, 0);      // 2 = 2 colunas para a direita. 0 = Primeira linha
  lcd.print("Desligando");
  lcd.setCursor(0, 1);
  lcd.print(" Projetor"); // Imprime um texto
  delay(1000);              // 5 segundos de delay

}
void msg_off_caixa()
{
  lcd.clear();              // Limpa o display
  lcd.setCursor(0, 0);      // 2 = 2 colunas para a direita. 0 = Primeira linha
  lcd.print("Desligando ");
  lcd.setCursor(0, 1);
  lcd.print(" caixa de Som"); // Imprime um texto
  delay(1000);              // 5 segundos de delay

}


void msg_inicio()
{
  lcd.clear();              // Limpa o display
  lcd.setCursor(0, 0);      // 2 = 2 colunas para a direita. 0 = Primeira linha
  lcd.print("Inicializando");
  lcd.setCursor(0, 1);
  lcd.print(" Sistema"); // Imprime um texto
  delay(1000);              // 5 segundos de delay

}

void msg_on_proj()
{
  lcd.clear();              // Limpa o display
  lcd.setCursor(0, 0);      // 2 = 2 colunas para a direita. 0 = Primeira linha
  lcd.print("Ligando ");
  lcd.setCursor(0, 1);
  lcd.print(" Projetor"); // Imprime um texto
  delay(1000);
}

void msg_on_caixa()
{
  lcd.clear();              // Limpa o display
  lcd.setCursor(0, 0);      // 2 = 2 colunas para a direita. 0 = Primeira linha
  lcd.print("Ativando  ");
  lcd.setCursor(0, 1);
  lcd.print(" Caixa de SOM"); // Imprime um texto
  delay(1000);
}

void msg_sistema()
{
  lcd.clear();              // Limpa o display
  lcd.setCursor(0, 0);      // 2 = 2 colunas para a direita. 0 = Primeira linha
  lcd.print("Sistema ");
  lcd.setCursor(0, 1);
  lcd.print("Iniciado"); // Imprime um texto
  delay(1000);
}

void msg_insira()
{
  lcd.clear();              // Limpa o display
  lcd.setCursor(0, 0);      // 2 = 2 colunas para a direita. 0 = Primeira linha
  lcd.print("Insira a ");
  lcd.setCursor(0, 1);
  lcd.print("Tag");
  delay(1000);
}

void desliga_tudo()
{
  msg_off();
  msg_off_proj();
  Serial.print("(PWR0)");  // envia comando serial para desligar Projetor
  digitalWrite(A1, LOW);   // desliga rele do projetor
  msg_off_caixa();
  digitalWrite(A0, LOW);  // desliga rele da caixa
  msg_sistemaoff();
  delay(1000);
}

void liga_tudo()
{
  msg_inicio(); // colocar msg de inicialzação do sistema
  msg_on_proj();
  Serial.print("(PWR1)"); // envia comando RS232 para ligar Projetor
  digitalWrite(A1, HIGH);  // liga rele do Projetor
  msg_on_caixa();
  digitalWrite(A0, HIGH);  // liga rele da caixa
  msg_sistema();
  delay(1000);
}

void msg_sistemaoff()
{
  lcd.clear();              // Limpa o display
  lcd.setCursor(0, 0);      // 2 = 2 colunas para a direita. 0 = Primeira linha
  lcd.print("Sistema ");
  lcd.setCursor(0, 1);
  lcd.print("Desligado"); // Imprime um texto
  delay(1000);
}

void desligamento_geral()

{
  msg_off();
  msg_off_proj();
  Serial.print("(PWR0)");  // envia comando serial para desligar Projetor
  digitalWrite(A1, LOW);   // desliga rele do projetor
  msg_off_caixa();
  digitalWrite(A0, LOW);  // desliga rele da caixa
  msg_sistemaoff();
  delay(1000);
  teste1();

}

void inicio()
{
  msg_inicio(); // colocar msg de inicialzação do sistema
  msg_on_proj();
  Serial.print("(PWR1)"); // envia comando RS232 para ligar Projetor
  digitalWrite(A1, HIGH);  // liga rele do Projetor
  msg_on_caixa();
  digitalWrite(A0, HIGH);  // liga rele da caixa
  msg_sistema();                                                                         //   mfrc522.PICC_HaltA();
  contador = 2; // Altera valor do contador, mudando o comportamento do laço

}

void loop()
{ // Inicia o laço
 do { // executa esse laço uma vez e continua apenas se o contador for maior ou igual a 2
    int contador = 1;
    if(mfrc522.PICC_IsNewCardPresent()){ // Se cartão esta presente
      inicio(); // Liga os aparelhos e soma 1 ao contador, liberando a segunda parte do loop
      break;
   }
   else{ // Se o cartão não está presente
      msg_insira(); // Mantem a msg de inserir, e para, pois o contador é = 1, até que o loop seja iniciado com o cartão.
   }
 } while (contador >= 2) { // Se o loop foi iniciado e o contador agora é 2 ou mais
    if(mfrc522.PICC_IsNewCardPresent()){
      // verifica se cartão continua inserido.
      msg_sistema();
    }
    else {
      // Se o cartão for removido após o inicio do sistema, ativa-se o desligamento geral.
      desligamento_geral();
    }

 }

}